---
title: pdfgrep - contact
header: Contact
---
<div class="content">
## Mailinglist

Our mailinglist <pdfgrep-users@pdfgrep.org> is used for just about
anything.

Feel free to ask questions about pdfgrep here, as well as report bugs
or send patches! It will also contain official announcements and is
generally very low traffic.

You can subscribe by either sending an email with subject `subscribe`
to [pdfgrep-users-request@pdfgrep.org] or using the [online formular].

There is also an [archive of all old messages]. 

## Bugtracker and Pull Requests

If you'd rather use a bugtracker or create pull requests, then the
[project page on GitLab<span class="icon external-link-icon"><i class="fa fa-external-link" aria-hidden="true"></i></span>](https://gitlab.com/pdfgrep/pdfgrep) is for
you.

[online formular]: https://lists.pdfgrep.org/mailman/listinfo/pdfgrep-users
[archive of all old messages]: https://lists.pdfgrep.org/pipermail/pdfgrep-users/
[pdfgrep-users-request@pdfgrep.org]: mailto:pdfgrep-users-request@pdfgrep.org?subject=subscribe

</div>

---
title: pdfgrep 2.0.1 released
published: 2017-03-06
author: Hans-Peter Deifel
---

pdfgrep 2.0.1 has been released! It contains only one bugfix for the
new `--cache` option from 2.0: When used together with recursive
search, `--cache` failed to index files in subdirectories. Thanks to
Barna Ágoston for the report.

As usual, the tarball is available on
the [download page](/download.html).

---
title: Good Bye SourceForge
published: 2015-06-19
author: Hans-Peter Deifel
---

SourceForge's aggressive advertising has always been frustrating, but
free alternatives that provide a mailing list were scarce. However,
recent [events] have made it intolerable.

Because of this, pdfgrep immediately switches to new infrastructure:

  - The new official website is now [pdfgrep.org](/)
  - All tarballs are now hosted on [pdfgrep.org/download.html](/download.html)
  - The mailinglist has been migrated to <pdfgrep-users@pdfgrep.org>.
    See [Contact](/contact.html) for details.

Please do not use pdfgrep's SourceForge page any more, in particular
don't download the tarballs from there.

A big thanks to [Christoph](https://christoph-egger.org/) for kindly
providing the hosting!

[events]: http://arstechnica.com/information-technology/2015/05/sourceforge-grabs-gimp-for-windows-account-wraps-installer-in-bundle-pushing-adware/

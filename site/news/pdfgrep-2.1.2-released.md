---
title: pdfgrep 2.1.2 released
published: 2018-11-19
author: Hans-Peter Deifel
---

Another little bugfix release that fixes a crash when built with
hardened compiler flags, which was specifically affecting Fedora
users. See [pdfgrep bug #31][1] and
<https://bugzilla.redhat.com/show_bug.cgi?id=1648154> for details. The
[download page] once again has the release tarball.

[1]: https://gitlab.com/pdfgrep/pdfgrep/issues/31
[download page]: /download.html

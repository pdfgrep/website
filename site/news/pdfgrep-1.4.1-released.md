---
title: pdfgrep 1.4.1 released
published: 2015-09-26
author: Hans-Peter Deifel
---

pdfgrep 1.4.1 is now released and can be [obtained](/download.html) in
the usual place.

This is a bugfix release, with the notable addition of a test suite
that can be run from the toplevel source directory with:

    make check

This test suite has already found some nasty bugs, which are now all
fixed. See the `NEWS` file for detailed information.

As usual, thanks to everyone who contributed!

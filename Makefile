CABAL=cabal run pdfgrepsite --

.PHONY: build clean watch deploy

build:
	$(CABAL) build

clean:
	$(CABAL) clean

watch:
	$(CABAL) watch

deploy:
	$(CABAL) deploy
